import React from "react";
import ReactDOM from "react-dom";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import reduxThunk from "redux-thunk";

import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";

import App from "./components/App";
import reducers from "./reducers";

const store = createStore(
  reducers, //Todos los reducers
  {}, //Estado inical
  applyMiddleware(reduxThunk)
);
const container = document.getElementById("app");
ReactDOM.render(
  <Provider store={store}>
    <App></App>
  </Provider>,
  container
);
