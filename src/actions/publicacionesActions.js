import axios from "axios"
import { TRAER_POR_USUARIO, CARGANDO, ERROR } from "../types/publicacionesTypes";
import * as usuariosTypes from "../types/usuariosTypes"

const { TRAER_TODOS: USUARIOS_TRAER_TODOS } =  usuariosTypes

export const traerPorUsuario = (key) => async(dispatch, getState) =>{

    dispatch({
        type:CARGANDO
    })
    const { usuarios } = getState().usuariosReducer
    const { publicaciones } = getState().publicacionesReducer
    
    const usuarios_id = usuarios[key].id
    try {
        const respuesta = await axios.get(`https://jsonplaceholder.typicode.com/posts/?userId=${usuarios_id}`)
        
        const publicacionesActualizadas = [
            ...publicaciones,
            respuesta.data
        ]
        
        dispatch({
            type: TRAER_POR_USUARIO,
            payload: publicacionesActualizadas
        })

        const pulicacionesKey =  publicacionesActualizadas.length - 1
        const usuariosActualizados = [...usuarios]
        usuariosActualizados[key] = {
            ...usuarios[key],
            pulicacionesKey
        }


        dispatch({
            type:USUARIOS_TRAER_TODOS,
            payload: usuariosActualizados
        })        
    } catch (error) {
        dispatch({
            type:ERROR,
            payload: 'publicaciones no disponibles'
        })
    }

}