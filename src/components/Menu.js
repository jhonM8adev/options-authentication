import React from "react";
import { Link } from "react-router-dom";

const Menu = props => (
  <nav id="menu">
    <Link to="/redux/usuarios">Usuarios</Link>
    <Link to="/redux/tareas">Tareas</Link>
  </nav>
);

export default Menu;
