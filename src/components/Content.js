import React from "react";
import { Link } from "react-router-dom";
import "./Style/Content.css";

class Content extends React.Component {
  render() {
    return (
      <div>
        <div className="content">
          <div className="bg-white p-3 h-100">
            <div className="container h-100 pr-0">
              <div>
                <div className="pb-3 row">
                  <div className="pl-lg-3 pr-lg-2 col-lg-7 col-xl-6">
                    <section className="shadow p-3 mb-5 bg-white rounded">
                      <h3 className="p-3 bg-white rounded h-100">
                        New Customer
                      </h3>
                      <h3 className="font-weight-light">
                        I want to register as a brand new customer. i've never
                        had a utility or broadband account with you in the past.
                      </h3>
                      <div className="row">
                        <div className="col-12 col-sm-6">
                          <div className="px-0 col-12">
                            <Link to="/" role="button">
                              Register
                            </Link>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                  <div className="pl-lg-2 pr-lg-3 col-lg-5 col-xl-6">
                    <section className="shadow p-3 mb-5 bg-white rounded">
                      <h3 className="p-3 bg-white rounded h-100">
                        Existing Customer
                      </h3>
                      <h3 className="font-weight-light">
                        I currently have a utility or broadband account with
                        your or l've been a customer in the past.
                      </h3>
                      <div className="row">
                        <div className="col-12 col-sm-6">
                          <div className="px-0 col-12">
                            <Link to="/" role="button">
                              Locate my account
                            </Link>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="button_back_summary">
          <button type="button" className="btn btn-outline-primary">
            Back to summary
          </button>
        </div>
      </div>
    );
  }
}

export default Content;
