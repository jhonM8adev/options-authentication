import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
/* import Options from "../pages/Options";
import Form from "../pages/Form"; */
import ReduxUsuers from "../pages/Usuarios";
import ReduxTareas from "../components/Tareas";
import ReduxPublicaciones from "../pages/publicaciones"
import Menu from "../components/Menu";

function App() {
  return (
    <BrowserRouter>
      {/* <Route exact path="/options" component={Options}></Route>
        <Route exact path="/form" component={Form}></Route> */}
      <Menu></Menu>
      <div>
        <Route exact path="/redux/usuarios" component={ReduxUsuers}></Route>
        <Route exact path="/redux/tareas" component={ReduxTareas}></Route>
        <Route exact path="/publicaciones/:key" component={ReduxPublicaciones}></Route>
      </div>
    </BrowserRouter>
  );
}

export default App;
