import React from "react";
import logo from "../images/logo.png";
import { Link } from "react-router-dom";

class NavBar extends React.Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-md navbar-light bg-white p-0 justify-content-center justify-content-md-between shadow-sm">
          <div className="container">
            <div className="col-12 col-md-4">
              <div className="row">
                <div className="col-md-12 col-8 offset-2 offset-md-0 px-0 d-flex justify-content-center align-items-center d-md-block">
                  <Link to="/">
                    <img
                      className="img-fluid"
                      src={logo}
                      style={{ maxHeight: 52 }}
                      alt=""
                    ></img>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

export default NavBar;
