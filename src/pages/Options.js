import React from "react";
import NavBar from "../components/NavBar";
import Content from "../components/Content";

class Options extends React.Component {
  render() {
    return (
      <div>
        <NavBar></NavBar>
        <Content></Content>
      </div>
    );
  }
}

export default Options;
