import React from "react";
import { connect } from "react-redux";
import * as usuariosActions from "../actions/usuariosActions";
import Spinner from "../components/general/Spinner";
import Fatal from "../components/general/Fatal";
import Tabla from "../components/Tabla";
import "../components/Style/Table.css";

class Usuarios extends React.Component {
  componentDidMount() {
    if(!this.props.usuarios.length){

      this.props.traerTodos();
    }
  }

  ponerContenido = () => {
    if (this.props.cargando) {
      return <Spinner></Spinner>;
    }
    if (this.props.error) {
      return <Fatal message={this.props.error}></Fatal>;
    }
    return <Tabla></Tabla>;
  };

  render() {
    return (
      <div className="margen">
        <h1>Usuarios</h1>
        {this.ponerContenido()}
      </div>
    );
  }
}

const mapStateToProps = reducers => {
  return reducers.usuariosReducer;
};

export default connect(
  mapStateToProps,
  usuariosActions
)(Usuarios);
