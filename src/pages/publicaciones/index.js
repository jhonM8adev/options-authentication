import React from "react"
import { connect } from "react-redux"
import Cargando from "../../components/general/Spinner"
import Fatal from "../../components/general/Fatal"
import * as usuariosActions from "../../actions/usuariosActions"
import * as publicacionesActios from "../../actions/publicacionesActions"
import "../../components/Style/publicaciones.css"

const { traerTodos:usuariosTraerTodos } = usuariosActions
const { traerPorUsuario:publicacionesTraerPorUsuario } = publicacionesActios   

class Publicaciones extends React.Component{
    async componentDidMount(){
        const {
            publicacionesTraerPorUsuario,
            match : {params : {key}}
        } = this.props
        if(!this.props.usuariosReducer.usuarios.length){
            await this.props.usuariosTraerTodos()
        }

        if(this.props.usuariosReducer.error){
            return 
        }

        if(!('pulicacionesKey' in this.props.usuariosReducer.usuarios[key])){

           publicacionesTraerPorUsuario(key)
        }
    }

    ponerUsuario = () =>{
        const { 
            usuariosReducer,
            match : {params : {key}}
        } = this.props

        if(usuariosReducer.error){
            return <Fatal message={usuariosReducer.error}></Fatal>
        }
        if(!usuariosReducer.usuarios.length ||  usuariosReducer.cargando){
            return <Cargando></Cargando>
        }
        const nombre = usuariosReducer.usuarios[key].name
        return <h1>Publicaciones de : {nombre}</h1>
    }


    ponerPublicaciones = () =>{
        const {
            usuariosReducer,
            usuariosReducer: { usuarios },
            publicacionesReducer,
            publicacionesReducer : { publicaciones },
            match : {params : {key}}
        } = this.props

        if(!usuarios.length) return
        if(usuariosReducer.error) return
        if(publicacionesReducer.cargando){
            return <Cargando></Cargando>
        } 
        if(publicacionesReducer.error) {
            return <Fatal message={publicacionesReducer.error}></Fatal>
        }
        if(!publicaciones.length) return
        if(!('pulicacionesKey' in usuarios[key])) return
        
        const { pulicacionesKey } = usuarios[key]
        return publicaciones[pulicacionesKey].map((publicacion) =>(
            <div 
                className="pub_titulo"
                key={publicacion.id}
                onClick= { ()=>alert(publicacion.id) } 
            >
                
                <h2>
                    { publicacion.title }
                </h2>
                <h3>
                    { publicacion.body }
                </h3>
            </div>
        ))
    }


    render(){
        console.log(this.props)
        return(
            <div>
                {this.ponerUsuario()}
                {this.ponerPublicaciones()}
            </div>
        )
    }
}

const mapStateToProps = ({usuariosReducer, publicacionesReducer}) =>{
    return {usuariosReducer, publicacionesReducer}
}

const mapDispatchToProps = {
    usuariosTraerTodos,
    publicacionesTraerPorUsuario
}
export default connect(mapStateToProps, mapDispatchToProps)(Publicaciones);

