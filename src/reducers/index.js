import { combineReducers } from "redux";
import usuariosReducer from "./usuariosReducers";
import publicacionesReducer from "./publicacionesReducers"

export default combineReducers({
  usuariosReducer, publicacionesReducer
});
